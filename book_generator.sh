#!/bin/bash

pandoc metadata.md ChroniclesRPG.md \
	-H TeX/head.tex \
	--toc --toc-depth=2 \
	-o 'Chronicles_Core'.pdf \
	--pdf-engine=xelatex

pandoc metadata.md ChroniclesRPG.md \
	--toc \
	-o 'Chronicles_Core.epub'

pandoc metadata.md ChroniclesRPG.md \
	--toc \
	-o 'Chronicles_Core.html'

